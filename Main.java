package donothing;

import java.util.Scanner;

// This program shows basic user input and input validation in Java.
public class Main
{
    // Scanner object for reading standard input (stdin) 
    private static final Scanner sc = new Scanner(System.in);
    // Main method
    public static void main(String[] args) 
    {
        // Greet the user and explain the use of the program.
        System.out.println("Welcome to donothing. This program does not do anything.");
        // Indicate user input is needed
        System.out.println("Enter your selection: ");
        // Give the user a list of selections
        System.out.print("1. Do Nothing\n2. Do Nothing\n3. Do Nothing\nChoose: ");
        // Let the user input their selection
        int input = sc.nextInt();
        // If the user inputs a number less than the given selections, an IllegalArgumentException will be thrown
        // with a custom error message.
        if(input < 1) throw new IllegalArgumentException("Invalid input. Exiting with failure status.");
        // Otherwise if the user inputs a number more than the given selections, an IllegalArgumentException will be thrown
        // with a custom error message.
        else if(input > 3) throw new IllegalArgumentException("Invalid input. Exiting with failure status.");
        // Otherwise output what the user selected and exit the program.
        else System.out.println("This program does not do anything. Goodbye.");
    }
}